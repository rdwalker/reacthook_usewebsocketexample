### useWebSocket

An example React Hook to open a webSocket and allow sending and receiving messages.
Automatically attempts to reconnect up to ten-times.

### Installation
1. Clone/fork this repository.
2. From the project directory, `npm install`

### How To Use
`npm run server` to start the nodeJS WebServer to create a socket and listen.

`npm start` to start the React App.js

Customize `useWebSocket.js` console.log messages as desired (or remove entirely);

### Example Usage
```
function MyComponent() {
  const [sendMessage, readyState, response] = useWebSocket('ws://localhost:8080');

  return (
    <div>
      <button
        onClick={sendMessage({userName: 'Dylan', content: 'Hello Server!'})}
        disabled={readyState !== 1}
      >
        Send Message
      </button>
      <div>
        {JSON.stringify(response)}
      </div>
    </div>
  );
}
```

### Prerequisites
* nodeJS v8+

### Author
R. Dylan Walker (dylan_walker@yahoo.com)

### TODOs
* Tests

### License
See [license.md](license.md)