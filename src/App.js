import React from 'react';
import useWebSocket from './useWebSocket';

function App() {
  const [sendMessage, readyState, response] = useWebSocket('ws://localhost:8080');

  return (
    <div>
      <div>
        {readyState}
      </div>
      <button
        onClick={sendMessage({userName: 'Dylan', content: 'Hello Server!'})}
        disabled={readyState !== 1}
      >
        Send Message
      </button>
      <div>
        {JSON.stringify(response)}
      </div>
    </div>
  );
}

export default App;
