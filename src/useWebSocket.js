import { useState, useLayoutEffect } from 'react';

function useWebSocket(url) {

  const [response, updateResponse] = useState(null);
  const [readyState, setReadyState] = useState(0);
  const [socket, setSocket] = useState(null);

  let ws = null;
  let connectionAttempts = 0;
  let maxAttempts = 10;

  useLayoutEffect (() => {
   wsConnect();
   return () => { ws.close(3001)};
  }, []);

  const wsConnect = () => {
    if (ws) {
      ws.close(3001);
    }
    ws = new WebSocket(url);

    ws.onopen = () => {
      console.log('Connection Made!');
      setReadyState(ws.readyState);
      setSocket(ws);
    };

    ws.onerror = (event) => {
      if (ws.readyState === 1) {
        console.log('Connection Error!', event.type);
      }
    };

    ws.onmessage = (event) => {
      receiveMessage(JSON.parse(event.data));
    };

    ws.onclose = (event) => {
      if (event.code === 3001) {
        console.log('Connect Closed!');
        ws = null;
        setReadyState(ws.readyState);
        setSocket(null);
      } else {
        console.log('No Connection - Attempting to Connect...');
        connectionAttempts += 1;
        console.log(`Connection Attempt ${connectionAttempts} of ${maxAttempts}`);

        setReadyState(ws.readyState);
        setSocket(null);

        if (connectionAttempts >= maxAttempts) {
          console.log('Could not connect to server!  Please make sure the server is running and try again.');
        } else {
          wsConnect();
        }
      }
    };
  };

  const sendMessage = (message) => () => {
    if (socket !== null && socket.readyState === 1) {
      socket.send(
        JSON.stringify(message)
      )
    }
  };

  const receiveMessage = (message) => {
    console.log(`${message.userName} is saying: ${message.content}`);
    updateResponse(message);
  };

  return [sendMessage, readyState, response]
}

export default useWebSocket;